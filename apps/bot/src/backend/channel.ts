import { apply_backend_headers } from "./base";
import { LoggingInstance as Logger } from "../logger";
import { get } from "superagent";

export const base_path = `/channels/`;

export type ChannelDatabaseRecord = {
  id: string;
  name: string;
  display: string;

  active: boolean;
  created_at: string;
};

export const get_channel_by_id = async (
  channelId: string
): Promise<ChannelDatabaseRecord | null> => {
  const endpoint = `${process.env.BACKEND_URI}/${base_path}/${channelId}`;
  try {
    const response = await apply_backend_headers(get(endpoint)).send();
    if (response.body && response.statusCode === 200) {
      return response.body as ChannelDatabaseRecord;
    }
  } catch (callError: any) {
    Logger.error(
      {
        ...callError,
        channelId,
        endpoint,
      },
      "Failed to get channel informations for given ID"
    );
  }
  return null;
};

export const get_channel_by_name = async (
  channelName: string
): Promise<ChannelDatabaseRecord | null> => {
  const endpoint = `${process.env.BACKEND_URI}/${base_path}`;
  try {
    const response = await apply_backend_headers(get(endpoint))
      .query({ name: channelName })
      .send();
    if (response.body && response.statusCode === 200) {
      return response.body as ChannelDatabaseRecord;
    }
  } catch (callError: any) {
    Logger.error(
      {
        ...callError,
        channelName,
        endpoint,
      },
      "Failed to get channel informations for given name"
    );
  }
  return null;
};

export const get_active_channels = async (): Promise<
  ChannelDatabaseRecord[]
> => {
  const endpoint = `${process.env.BACKEND_URI}/${base_path}`;
  try {
    const response = await apply_backend_headers(get(endpoint))
      .query({ active: 1 })
      .send();
    if (response.body && response.statusCode === 200) {
      return response.body as ChannelDatabaseRecord[];
    }
  } catch (callError: any) {
    Logger.error(
      {
        ...callError,
        endpoint,
      },
      "Failed to get active channels"
    );
  }
  return [];
};
