import { apply_backend_headers } from "./base";
import { LoggingInstance as Logger } from "../logger";
import { post } from "superagent";

export const base_path = `/activities/`;

export enum ActivityTypes {
  InGameNameRegistration = "!ign:registration",
  InGameNameDisplaying = "!ign:display",
  InGameNameSearch = "!igs:usage",
  InGameNameReverseSearch = "!igrs:usage",
  InGameNamePurge = "!ignpurge:usage",
}

export type ActivityRecord = {
  id: number;
  created_at: string;
  chatter_id: string;
  channel_id: string;
  activity_type: ActivityTypes;
  raw_message: string;
  raw_tags: string;
};
export type NewActivityRecord = Omit<ActivityRecord, "id" | "created_at">;

export const register_activity = async (
  details: NewActivityRecord
): Promise<void> => {
  const endpoint = `${process.env.BACKEND_URI}/${base_path}`;
  try {
    const response = await apply_backend_headers(post(endpoint)).send(details);
    if (response.statusCode !== 204) {
      throw Error("Unexpected status code from activities");
    }
  } catch (callError: any) {
    Logger.error(
      {
        ...callError,
        endpoint,
        activity: details,
      },
      "Failed to insert activity into the database"
    );
  }
};
