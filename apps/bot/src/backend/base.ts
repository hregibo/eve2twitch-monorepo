import { set_base_headers } from "../utils";
import { Request } from "superagent";

export const apply_backend_headers = (req: Request): Request => {
  return set_base_headers(req).auth(
    process.env.BACKEND_AUTH_TOKEN || "dev-jwt-token",
    { type: "bearer" }
  );
};
