import { apply_backend_headers } from "./base";
import { LoggingInstance as Logger } from "../logger";
import { get, post } from "superagent";
import { ESICharacterWithID } from "../esi";

export const base_path = `/characters/`;

export type CharacterGender = "male" | "female";
export type CharacterDatabaseRecord = ESICharacterWithID;

export const get_character_by_name = async (
  characterName: string
): Promise<CharacterDatabaseRecord | null> => {
  const endpoint = `${process.env.BACKEND_URI}/${base_path}`;
  try {
    const character = await apply_backend_headers(get(endpoint))
      .auth(process.env.BACKEND_AUTH_TOKEN || "dev-jwt-token", {
        type: "bearer",
      })
      .query({ name: characterName })
      .send();
    if (character.body && character.statusCode === 200) {
      return character.body as CharacterDatabaseRecord;
    }
  } catch (callError: any) {
    Logger.error(
      {
        ...callError,
        characterName,
        endpoint,
      },
      "Failed to obtain character informations for given name"
    );
  }
  return null;
};

export const get_character_by_id = async (
  characterId: number
): Promise<CharacterDatabaseRecord | null> => {
  const endpoint = `${process.env.BACKEND_URI}/${base_path}/${characterId}`;
  try {
    const character = await apply_backend_headers(get(endpoint))
      .auth(process.env.BACKEND_AUTH_TOKEN || "dev-jwt-token", {
        type: "bearer",
      })
      .send();
    if (character.body && character.statusCode === 200) {
      return character.body as CharacterDatabaseRecord;
    }
  } catch (callError: any) {
    Logger.error(
      {
        ...callError,
        characterId,
        endpoint,
      },
      "Failed to obtain character informations for given ID"
    );
  }
  return null;
};

export const upsert_character = async (
  details: CharacterDatabaseRecord
): Promise<CharacterDatabaseRecord | null> => {
  const endpoint = `${process.env.BACKEND_URI}/${base_path}`;
  try {
    const response = await apply_backend_headers(post(endpoint)).send(details);
    if (![200, 201].includes(response.statusCode)) {
      return null;
    }
    if (response.body) {
      return response.body as CharacterDatabaseRecord;
    }
  } catch (callError: any) {
    Logger.error(
      {
        ...callError,
        characterData: details,
        endpoint,
      },
      "Failed to insert a character into the database"
    );
  }
  return null;
};
