import { apply_backend_headers } from "./base";
import { LoggingInstance as Logger } from "../logger";
import { del, get, post } from "superagent";

export const base_path = `/chatters/`;

export type BroadcasterType = "partner" | "affiliate" | "";
export type TwitchUserType = "staff" | "admin" | "global_mod" | "";
export type ChatterRecord = {
  id: string;
  login: string;
  display_name?: string;
  created_at?: string;

  description?: string;
  profile_image_url?: string;

  broadcaster_type?: BroadcasterType;
  type?: TwitchUserType;
};

export const get_chatter_by_id = async (
  chatterId: string
): Promise<ChatterRecord | null> => {
  const endpoint = `${process.env.BACKEND_URI}/${base_path}/${chatterId}`;
  try {
    const response = await apply_backend_headers(get(endpoint)).send();
    if (response.body && response.statusCode === 200) {
      return response.body as ChatterRecord;
    }
  } catch (callError: any) {
    Logger.error(
      {
        ...callError,
        chatterId,
        endpoint,
      },
      "Failed to get chatter informations for given ID"
    );
  }
  return null;
};

export const get_chatter_by_name = async (
  chatterName: string
): Promise<ChatterRecord | null> => {
  const endpoint = `${process.env.BACKEND_URI}/${base_path}`;
  try {
    const response = await apply_backend_headers(get(endpoint))
      .query({ name: chatterName })
      .send();
    if (response.body && response.statusCode === 200) {
      return response.body as ChatterRecord;
    }
  } catch (callError: any) {
    Logger.error(
      {
        ...callError,
        chatterName,
        endpoint,
      },
      "Failed to get chatter informations for given name"
    );
  }
  return null;
};

export const upsert_chatter = async (
  details: ChatterRecord
): Promise<ChatterRecord | null> => {
  const endpoint = `${process.env.BACKEND_URI}/${base_path}`;
  try {
    const response = await apply_backend_headers(post(endpoint)).send(details);
    if (response.body && [200, 201].includes(response.statusCode)) {
      return response.body as ChatterRecord;
    }
  } catch (callError: any) {
    Logger.error(
      {
        ...callError,
        endpoint,
        chatter: details,
      },
      "Failed to upsert the chatter into the database"
    );
  }
  return null;
};

export const remove_chatter = async (chatterId: string): Promise<void> => {
  const endpoint = `${process.env.BACKEND_URI}/${base_path}/${chatterId}`;
  try {
    const response = await apply_backend_headers(del(endpoint)).send();
    if (response.statusCode === 204) {
      return;
    }
  } catch (callError: any) {
    Logger.error(
      {
        ...callError,
        endpoint,
        chatterId,
      },
      "Failed to delete the chatter from our the database"
    );
  }
  throw Error("Unexpected error while asking for a chatter deletion");
};
