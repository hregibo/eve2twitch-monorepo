import { apply_backend_headers } from "./base";
import { LoggingInstance as Logger } from "../logger";
import { CharacterDatabaseRecord, get_character_by_name } from "./character";
import { ChatterRecord, get_chatter_by_name } from "./chatter";
import { del, get, post } from "superagent";

export type RelationResponse = {
  chatter: ChatterRecord;
  character: CharacterDatabaseRecord;
};

export const get_character_by_chatter_id = async (
  chatterId: string
): Promise<CharacterDatabaseRecord | null> => {
  const endpoint = `${process.env.BACKEND_URI}/chatters/${chatterId}/character`;
  try {
    const response = await apply_backend_headers(get(endpoint)).send();
    if (response.body && response.statusCode === 200) {
      return response.body as CharacterDatabaseRecord;
    }
  } catch (callError: any) {
    Logger.info(
      {
        ...callError,
        endpoint,
        chatterId,
      },
      "Failed to get character informations for given chatter ID"
    );
  }
  return null;
};

export const get_character_by_chatter_name = async (
  chatterName: string
): Promise<CharacterDatabaseRecord | null> => {
  try {
    const chatter = await get_chatter_by_name(chatterName);
    if (!chatter) {
      return null;
    }
    return await get_character_by_chatter_id(chatter.id);
  } catch (callError: any) {
    Logger.info(
      {
        ...callError,
        chatterName,
      },
      "Failed to get character informations for given chatter ID"
    );
  }
  return null;
};

export const get_chatter_by_character_id = async (
  characterId: number
): Promise<ChatterRecord | null> => {
  const endpoint = `${process.env.BACKEND_URI}/characters/${characterId}/chatter`;
  try {
    const response = await apply_backend_headers(get(endpoint)).send();
    if (response.body && response.statusCode === 200) {
      return response.body as ChatterRecord;
    }
  } catch (callError: any) {
    Logger.info(
      {
        ...callError,
        endpoint,
        characterId,
      },
      "Failed to obtain chatter using the given character ID"
    );
  }
  return null;
};

export const get_chatter_by_character_name = async (
  characterName: string
): Promise<ChatterRecord | null> => {
  try {
    const character = await get_character_by_name(characterName);
    if (!character) {
      return null;
    }
    return await get_chatter_by_character_id(character.id);
  } catch (callError: any) {
    Logger.info(
      {
        ...callError,
        characterName,
      },
      "Failed to get chatter name based on given character name"
    );
  }
  return null;
};

export const register_character = async (
  chatterId: string,
  characterId: number
): Promise<RelationResponse | null> => {
  const endpoint = `${process.env.BACKEND_URI}/relations`;
  try {
    const response = await apply_backend_headers(post(endpoint)).send({
      chatter_id: chatterId,
      character_id: characterId,
    });
    if (response.body && [200, 201].includes(response.statusCode)) {
      return response.body as RelationResponse;
    }
  } catch (callError: any) {
    Logger.error(
      {
        ...callError,
        endpoint,
        chatterId,
        characterId,
      },
      "Failed to register the relation between the given chatter and character"
    );
  }
  return null;
};

export const purge_relation = async (chatter_id: string): Promise<void> => {
  const endpoint = `${process.env.BACKEND_URI}/relations`;
  try {
    const response = await apply_backend_headers(del(endpoint)).send({
      chatter_id,
    });
    if (response.statusCode === 204) {
      return;
    }
  } catch (callError: any) {
    Logger.error(
      {
        ...callError,
        endpoint,
        chatter_id,
      },
      "Failed to delete the relation between the given chatter and its assigned character"
    );
  }
  throw Error("Unexpected response from backend while deleting relation");
};
