import { MessageQueue } from "@hregibo/message-queue";
import Logger from "./logger";
const _channelCache = new Map<string, string>();

export const createBaseQueue = (queue: MessageQueue) => {
  queue.setQueue({
    delay: 3000,
    name: "base",
    priority: 1,
  });
};
export const createQueueForChannel = (queue: MessageQueue, channel: any) => {
  queue.setQueue({
    delay: 1550,
    name: channel.name,
    priority: 10,
  });
};
export const triggerJoinRequests = async (queue: MessageQueue) => {
  const channels: any = [
    /* ... */
  ];
  for (const channel of channels) {
    _channelCache.set(`#${channel.name}`, channel.id);
    Logger.debug(channel, "enqueueing the join request for this channel");
    queue.emit("enqueue", {
      message: `JOIN #${channel.name}`,
      priority: 10,
      queue: "base",
    });
    createQueueForChannel(queue, channel);
  }
};
