import {
  get_character_id_by_name,
  get_character_by_id,
} from "./../esi/character";
import { get_character_by_name } from "./../backend/character";
import {
  ChatterRole,
  get_command,
  is_user_allowed,
  parse_ign,
} from "./../utils";
import {
  get_character_by_chatter_id,
  get_character_by_chatter_name,
  get_chatter_by_character_name,
  purge_relation,
} from "./../backend/relation";
import { ActivityTypes, register_activity } from "../backend/activity";
import { channels_id_mapping } from "../cache/channels";
import { stringify_tags } from "../utils";
import { IParsedMessage } from "twsc";
import { upsert_character } from "../backend/character";

export const display_ign = async (message: IParsedMessage): Promise<string> => {
  const user_id = message.tags.get("user-id");
  if (!user_id) {
    throw Error("No user-id found in the tags of the message, exiting");
  }

  await register_activity({
    activity_type: ActivityTypes.InGameNameDisplaying,
    channel_id: channels_id_mapping.get(message.params[0].substring(1)) || "",
    chatter_id: user_id,
    raw_message: message.content || "",
    raw_tags: stringify_tags(message.tags),
  });

  const display_name =
    message.tags.get("display-name") || message.prefix.get("user") || "";

  const retrieved_character = await get_character_by_chatter_id(user_id);
  if (!retrieved_character) {
    return `@${display_name}: No IGN registered.`;
  }
  return `@${display_name}: IGN "${retrieved_character.name}"`;
};

export const register_ign = async (message: IParsedMessage) => {
  const user_id = message.tags.get("user-id");
  if (!user_id) {
    throw Error("No user-id found in the tags of the message, exiting");
  }

  await register_activity({
    activity_type: ActivityTypes.InGameNameDisplaying,
    channel_id: channels_id_mapping.get(message.params[0].substring(1)) || "",
    chatter_id: user_id,
    raw_message: message.content || "",
    raw_tags: stringify_tags(message.tags),
  });

  // we either get the display-name if available, or the user (login) name
  const display_name =
    message.tags.get("display-name") || message.prefix.get("user");
  const message_fragments = get_command(message.content || "");

  if (!display_name) {
    // This is basically close to impossible not to have any user details from
    // the IRC message, but hey, for the sake of code safety, I'll handle it.
    throw Error(
      "Unable to find any user login or display name for given message"
    );
  }

  const raw_character_name = message_fragments.slice(1).join();
  const parsed_character_name = parse_ign(raw_character_name);
  // fetch character in datase if any
  let character_informations = await get_character_by_name(
    parsed_character_name
  );
  // gets the character ID if the character_informations has been found
  let character_id: number | null = character_informations?.id || null;
  // in case this character does not exists in the local database, we search
  // its ID in the ESI API
  if (!character_id) {
    character_id = await get_character_id_by_name(parsed_character_name);
  }
  // in case the character ID is found, either via db or ESI API,
  // we fetch the latest informations about given user from ESI
  if (character_id) {
    character_informations = await get_character_by_id(character_id);
  }

  // TODO here we have an issue. If the user is found, everything is great.
  // But what if the input character name is wrong? We cannot, with the
  // current tables structure, store an invalid IGN into the characters
  // we would need, in the relation between a chatter and a character, set a
  // boolean "found_in_esi" or similar? To be defined.

  // in case the character was successfully found, we upsert the database
  if (character_informations) {
    await upsert_character(character_informations);
    return `@${display_name}: IGN "${character_informations.name}" registered.`;
  } else {
    // TODO based on prev. comment block, find a way to save the invalid IGN
    return `@${display_name}: IGN "${parsed_character_name}" registered but not found in EVE Online.`;
  }
};

export const search_ign = async (message: IParsedMessage) => {
  const chatter_id = message.tags.get("user-id");
  if (!chatter_id) {
    throw Error("No user-id found in the tags of the message, exiting");
  }

  await register_activity({
    activity_type: ActivityTypes.InGameNameSearch,
    channel_id: channels_id_mapping.get(message.params[0].substring(1)) || "",
    chatter_id: chatter_id,
    raw_message: message.content || "",
    raw_tags: stringify_tags(message.tags),
  });

  const chatter_name =
    message.tags.get("display-name") || message.tags.get("login") || "";
  const message_fragments = get_command(message.content || "");

  if (message_fragments.length <= 1) {
    return;
  }

  if (
    !is_user_allowed(message, ChatterRole.broadcaster | ChatterRole.moderator)
  ) {
    return `@${chatter_name}: Only broadcaster and mods can do that.`;
  }

  const target_chatter_name = message_fragments
    .slice(1)
    .join(" ")
    .replace("@", "");
  const target_chatter_found = await get_character_by_chatter_name(
    target_chatter_name
  );

  if (target_chatter_found) {
    return `@${chatter_name}: Account "${target_chatter_name}" registered IGN "${target_chatter_found.name}".`;
  } else {
    return `@${chatter_name}: Account "${target_chatter_name}" has not registered a IGN.`;
  }
};

export const reverse_search_ign = async (message: IParsedMessage) => {
  const chatter_id = message.tags.get("user-id");
  if (!chatter_id) {
    throw Error("No user-id found in the tags of the message, exiting");
  }

  await register_activity({
    activity_type: ActivityTypes.InGameNameReverseSearch,
    channel_id: channels_id_mapping.get(message.params[0].substring(1)) || "",
    chatter_id: chatter_id,
    raw_message: message.content || "",
    raw_tags: stringify_tags(message.tags),
  });

  const chatter_name =
    message.tags.get("display-name") || message.tags.get("login") || "";
  const message_fragments = get_command(message.content || "");

  if (message_fragments.length <= 1) {
    return;
  }

  if (
    !is_user_allowed(message, ChatterRole.broadcaster | ChatterRole.moderator)
  ) {
    return `@${chatter_name}: Only broadcaster and mods can do that.`;
  }

  const character_target_name = parse_ign(message_fragments.slice(1).join(" "));
  const target_chatter_found = await get_chatter_by_character_name(
    character_target_name
  );
  if (target_chatter_found) {
    return `@${chatter_name}: IGN "${character_target_name}" belongs to "${
      target_chatter_found.display_name || target_chatter_found.login
    }" (${target_chatter_found.login}).`;
  }
  return `@${chatter_name}: IGN "${character_target_name}" not registered.`;
};

export const purge_ign = async (message: IParsedMessage) => {
  const chatter_id = message.tags.get("user-id");
  if (!chatter_id) {
    throw Error("No user-id found in the tags of the message, exiting");
  }

  const chatter_display_name =
    message.tags.get("display-name") || message.tags.get("user") || "";

  await register_activity({
    activity_type: ActivityTypes.InGameNamePurge,
    channel_id: channels_id_mapping.get(message.params[0].substring(1)) || "",
    chatter_id: chatter_id,
    raw_message: message.content || "",
    raw_tags: stringify_tags(message.tags),
  });

  try {
    await purge_relation(chatter_id);
    return `@${chatter_display_name}: Successfully purged the character bound to your Twitch account.`;
  } catch (error: any) {
    return `@${chatter_display_name}: An error occured while purging the character.`;
  }
};
