import { LoggingInstance as Logger } from "../logger";
import { get, Request } from "superagent";
import { set_base_headers } from "../utils";
import { base_uri } from "./base";

export interface ESICharacter {
  alliance_id?: number;
  birthday?: string;
  bloodline_id?: number;
  corporation_id?: number;
  description?: string;
  faction_id?: number;
  gender?: "male" | "female";
  name: string;
  race_id?: number;
  security_status?: number;
  title?: string;
}

export type ESICharacterWithID = ESICharacter & { id: number };
export enum DataSource {
  TQ = "tranquility",
  SISI = "singularity",
}

export const apply_esi_headers = (request: Request): Request => {
  return set_base_headers(request).set(
    "user-agent",
    `EVE2TWITCH-${process.env.ESI_APP_ID}; Contact: IGN 'Velia Deviluke'`
  );
};

export const get_character_by_id = async (
  characterId: number
): Promise<ESICharacterWithID | null> => {
  const uri = `${base_uri}/characters/${characterId}`;
  try {
    const response = await apply_esi_headers(get(uri))
      .query({ datasource: DataSource.TQ })
      .send();

    if (response.body && response.statusCode === 200) {
      return { ...response.body, id: characterId } as ESICharacterWithID;
    }
  } catch (httpFailure: any) {
    Logger.error(
      {
        ...httpFailure,
        characterId,
      },
      `Failed to obtain character with given id`
    );
  }
  return null;
};

export const get_character_id_by_name = async (
  characterName: string
): Promise<number | null> => {
  const uri = `${base_uri}/search/`;

  try {
    const response = await apply_esi_headers(get(uri))
      .query({
        categories: "character",
        datasource: DataSource.TQ,
        language: "en",
        strict: "true",
        characterName,
      })
      .send();
    if (response.body) {
      const foundCharacters = response.body.character;
      if (Array.isArray(foundCharacters) && foundCharacters.length > 0) {
        return foundCharacters[0] as number;
      }
    }
  } catch (httpFailure: any) {
    Logger.error(
      {
        ...httpFailure,
        characterName,
      },
      `Failed to obtain character with given IGN`
    );
  }
  return null;
};
