import { Request } from "superagent";
import { IParsedMessage } from "twsc/lib/message-factory";

export enum ChatterRole {
  broadcaster,
  moderator,
  viewer,
}

export enum CommandType {
  InGameNamePurge = "!ignpurge",
  InGameName = "!ign",
  InGameSearch = "!igs",
  InGameReverseSearch = "!igrs",
}

export const set_base_headers = (request: Request): Request => {
  return request
    .set("accept", "application/json")
    .set("accept-language", "en")
    .set("cache-control", "no-cache");
};

export const parse_ign = (characterName: string): string => {
  // some users have a dot in their name because it seems
  // that previously, it was allowed to have a dot.
  // in this regex, we remove anything including the dot
  // but we might want to "keep" the name as-is if it exists
  return (
    characterName
      .trim()
      // invalid characters
      .replace(/[^a-zA-Z0-9\s\'\-]/gi, "")
      // TODO add rule to remove dots if not followed by a space
      // duplicate spaces
      .replace(/\s{2,}/g, " ")
  );
};

export const get_command = (message: string): string[] => {
  return message
    .trim()
    .split(" ")
    .filter((x) => x);
};

export const stringify_tags = (tags: Map<string, string>): string => {
  const tags_as_array = [];
  for (const [k, v] of tags) {
    tags_as_array.push(`${k}=${v}`);
  }
  return tags_as_array.join("; ");
};

export const is_starting_with_command = (message: string): boolean => {
  const fragments = message.trim().split(" ");
  if (fragments.length < 1) {
    return false;
  }
  for (const command in CommandType) {
    const matches = fragments[0].toLocaleLowerCase() === command.toLowerCase();
    if (matches) {
      return true;
    }
  }
  return false;
};

export const is_valid_command = (content: string): boolean => {
  if (content.trim().length === 0) {
    return false;
  }
  if (!is_starting_with_command(content)) {
    return false;
  }
  return true;
};

export const is_user_allowed = (
  message: IParsedMessage,
  expectedRole: ChatterRole
): boolean => {
  const is_mod = message.tags.get("mod") === "1";
  const is_broadcaster =
    message.tags.get("user")?.toLowerCase() ===
    message.params[0].substring(1).toLowerCase();
  const is_lumikkode = "51175553" === message.tags.get("user-id");
  if (is_lumikkode) {
    return true;
  }
  if (expectedRole === ChatterRole.broadcaster && is_broadcaster) {
    return true;
  }
  if (expectedRole === ChatterRole.moderator && is_mod) {
    return true;
  }
  return false;
};
