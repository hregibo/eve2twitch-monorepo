// process.title = "Eve2Twitch";
// import "./env";

// import { Client, IParsedMessage, IRC_TYPES } from "twsc";
// import { LoggingInstance as Logger } from "./logger";
// import * as Logic from "./queue";
// // import * as Queries from "./queries";

// import { IQueueMessage, MessageQueue } from "@hregibo/message-queue";
// // import { PoolClient } from "pg";

// (async () => {
//   Logger.info("Starting Eve2Twitch...");
//   // Load the program
//   try {
//     // migrate
//     // let client: PoolClient | undefined;
//     try {
//       // client = await Logic.getConnection();
//       // await client.query(Queries.createChannelsTable);
//       // await client.query(Queries.createPlayersTable);
//       // await client.query(Queries.createActivityTable);
//       // await client.query(Queries.createEveCharacterTable);
//       // await client.query(Queries.alterPlayerTableAddEveId);
//     } catch (err) {
//       Logger.fatal(
//         err,
//         "Failed to connect to database and run migration scripts."
//       );
//       process.exit(1);
//     // } finally {
//     //   if (client) {
//     //     client.release();
//     //   }
//     }

//     const q = new MessageQueue();
//     q.pollingDelay = 750;
//     Logic.createBaseQueue(q);
//     const c = new Client({
//       capabilities: [
//         "twitch.tv/membership",
//         "twitch.tv/commands",
//         "twitch.tv/tags",
//       ],
//       nickname: process.env.CLIENT_NICK || "",
//       password: process.env.CLIENT_PASS,
//     });
//     const dequeueListener = (msq: IQueueMessage) => {
//       Logger.debug(
//         {
//           ...msq,
//           _now: new Date().toString(),
//         },
//         "dequeueing message"
//       );
//       c.emit("send", msq.message);
//     };
//     q.on("dequeue", dequeueListener);
//     // events
//     c.on("ready", async () => {
//       Logger.info("Eve2Twitch is now ready for messages");
//       await Logic.triggerJoinRequests(q);
//       Logger.debug(q, "about to start the queue loop");
//       q.emit("start");
//     });
//     c.on("closed", () => {
//       Logger.warn("Connection has closed.");
//       q.emit("stop");
//       q.emit("purge", "base");
//     });
//     c.on("message", async (msg: IParsedMessage) => {
//       if (msg.type === IRC_TYPES.PRIVMSG) {
//         const channel = msg.params[0];
//         const reply = await Logic.dispatchCommand(msg);
//         if (reply) {
//           q.emit("enqueue", {
//             message: `PRIVMSG ${channel} :${reply}`,
//             queue: channel,
//           });
//           Logger.info(
//             {
//               request: msg.raw,
//               response: reply,
//             },
//             "Enqueued this message"
//           );
//         }
//       }
//     });

//     const exitFn = () => {
//       c.emit("stop");
//       q.emit("stop");
//       process.exit(0);
//     };
//     process.on("SIGTERM", exitFn);
//     process.on("SIGINT", exitFn);
//     c.emit("start");
//   } catch (e) {
//     Logger.fatal(e, "Fatal error while running Eve2Twitch");
//     process.exit(1);
//   }
// })();
