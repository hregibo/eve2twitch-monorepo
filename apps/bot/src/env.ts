import { config } from "dotenv";
import { require_env } from "@hregibo/require-env";
import { resolve } from "path";

config({
  path: resolve(__dirname, "../.env"),
});

require_env(["DB_STRING"]);
