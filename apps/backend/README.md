# EVE2TWITCH Backend

This folder holds the backend application for the `EVE2TWITCH` bot.
The goal of this backend is to serve as a middleware between the bot and the database, as well as an endpoint for the frontend, allowing users to authenticate and access their informations using the dashboard.

## Required environment variables

| Name                      | Description                                                                               |
| ------------------------- | ----------------------------------------------------------------------------------------- |
| `PSQL_CONNECTION_STRING`  | The connection string to log into the PostgreSQL Database and read/write informations     |
| `REDIS_CONNECTION_STRING` | The connection string to authenticate with Redis and use it as a cache and Pub/Sub system |

## Accessing the application

to be defined.
