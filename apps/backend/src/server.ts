import { require_env } from "@hregibo/require-env";
import { server, ServerOptions } from "@hapi/hapi";
import { randomUUID } from "crypto";

require_env("SERVER_PORT");
declare module "@hapi/hapi" {
  interface RequestApplicationState {
    transactionId?: string;
  }
}

export const server_options: ServerOptions = {
  port: process.env.SERVER_PORT ?? 8080,
  host: "::",
  router: {
    isCaseSensitive: false,
    stripTrailingSlash: true,
  },
};

export const api = server(server_options);
api.ext("onPreHandler", (req, h, err) => {
  if (err) {
    api.log(["error"], err);
  }
  req.app.transactionId = randomUUID();
  return h.continue;
});
