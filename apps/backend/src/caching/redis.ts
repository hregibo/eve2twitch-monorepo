import { createClient, RedisClientType } from "redis";
import { require_env } from "@hregibo/require-env";

require_env("REDIS_CONNECTION_STRING");

export const service: RedisClientType = createClient({
  url: process.env.REDIS_CONNECTION_STRING,
});

export const invalidate = async (prefix: string, key: string) => {
  return await service.del(`${prefix}:${key}`);
};

export const register = async (
  prefix: string,
  key: string,
  value: string,
  ttl: number = 3600
) => {
  const rediskey = `${prefix}:${key}`;
  const ret = await service.set(rediskey, value);
  await service.expire(rediskey, ttl);
  return ret;
};

export const read = async (prefix: string, key: string) => {
  return await service.get(`${prefix}:${key}`);
};
