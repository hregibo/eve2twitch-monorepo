import { api } from "./server";
import { routes } from "./services/characters";
import { esi } from "./services/authentication";

(async () => {
  await api.start();
  await api.register(esi, {
    routes: {
      prefix: "/authentication/esi",
    },
  });
  api.route(routes);
  api.route({
    method: "*",
    path: "/",
    handler: (_req, resp) => {
      return resp.response(process.cpuUsage()).code(200);
    },
  });
})();
