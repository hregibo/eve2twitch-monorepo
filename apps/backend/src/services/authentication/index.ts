import { Plugin } from "@hapi/hapi";
import { plugin as esi_plugin } from "./esi";

export * as esi from "./esi";
export * as jwt from "./jwt";
export * as twitch from "./twitch";

export const plugins: Plugin<any>[] = [esi_plugin];
