import { SuperAgentRequest, post, get } from "superagent";
import {
  ESIToken,
  ESITokenValidity,
  ThirdPartyHealthcheck,
} from "./interfaces";

export const BASE_URI: string = "https://esi.evetech.net/latest";
export const OAUTH_URI: string = "https://login.eveonline.com/v2/oauth";
export const getEsiStateKey = (state: string) => {
  return `state:esi:${state}`;
};

export const fetch_token = async (code: string | string[]) => {
  const basicAuth = Buffer.from(
    `${process.env.ESI_CLIENT || ""}:${process.env.ESI_SECRET || ""}`
  ).toString("base64");
  const oauthResult = await post(`${OAUTH_URI}/token`)
    .set("Authorization", `Basic ${basicAuth}`)
    .set("content-type", "application/x-www-form-urlencoded")
    .send({
      grant_type: "authorization_code",
      code,
    });
  return oauthResult.body as ESIToken;
};

export const prepare_query = async (
  query: SuperAgentRequest
): Promise<SuperAgentRequest> => {
  return query
    .set("accept", "application/json")
    .set("accept-language", "en")
    .set("cache-control", "no-cache")
    .set(
      "user-agent",
      `eve2twitch - ${process.env.ESI_CLIENT} (Velia Deviluke; eve2twitch@lumikko.dev)`
    );
};

export const verify_credentials = async (
  auth: ESIToken
): Promise<ESITokenValidity> => {
  const detailsRequest = await get(`https://esi.evetech.net/verify/`)
    .query({
      datasource: "tranquility",
    })
    .set("Authorization", `Bearer ${auth.access_token}`)
    .send();
  return detailsRequest.body as ESITokenValidity;
};

export const refresh_token = async (auth: ESIToken) => {
  const basicAuth = Buffer.from(
    `${process.env.ESI_CLIENT || ""}:${process.env.ESI_SECRET || ""}`
  ).toString("base64");
  const oauthResult = await post(`${OAUTH_URI}/token`)
    .set("Authorization", `Basic ${basicAuth}`)
    .set("content-type", "application/x-www-form-urlencoded")
    .send({
      grant_type: "refresh_token",
      refresh_token: auth.refresh_token,
    });
  return oauthResult.body as ESIToken;
};

export const esi_healthcheck = async (): Promise<ThirdPartyHealthcheck> => {
  const endpoint = `${BASE_URI}/status/?datasource=tranquility`;
  const esi: ThirdPartyHealthcheck = {
    endpoint,
    alive: true,
    latency: null,
    error: null,
  };
  try {
    const before = Date.now();
    await get(endpoint);
    esi.latency = Date.now() - before;
  } catch (error: any) {
    esi.alive = false;
    esi.error = error.message;
  }
  return esi;
};
