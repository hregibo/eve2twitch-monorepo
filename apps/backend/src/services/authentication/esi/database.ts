import { query } from "@hregibo/pg-dbc";
import { ESIToken } from "./interfaces";

export const query_insert_token =
  "INSERT INTO tokens_esi (access_token, expires_in, token_type, refresh_token, character_id) VALUES ($1, $2, $3, $4, $5);";
export const query_get_token =
  "SELECT access_token, expires_in, token_type, refresh_token, character_id FROM tokens_esi WHERE character_id = $1 LIMIT 1;";

export const store_token = async (token: ESIToken, character_id: number) => {
  await query<ESIToken>(query_insert_token, [
    token.access_token,
    token.expires_in,
    token.token_type,
    token.refresh_token,
    character_id,
  ]);
};

export const get_token = async (character_id: string) => {
  return await query<ESIToken>(query_get_token, [character_id]);
};
