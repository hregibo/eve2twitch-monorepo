import { Plugin } from "@hapi/hapi";
import { require_env } from "@hregibo/require-env";
import { routes } from "./routes";

export const plugin: Plugin<any> = {
  name: "authentication-esi",
  register: async (server, _options) => {
    require_env(["ESI_CLIENT_ID", "ESI_CLIENT_SECRET", "ESI_REDIRECT_URI"]);
    server.route([...routes]);
  },
};
export default plugin;
