export interface ESIToken {
  access_token: string;
  expires_in: number;
  token_type: "Bearer";
  refresh_token: string;
}

export interface ESITokenValidity {
  CharacterID: number;
  CharacterName?: string;
  ExpiresOn?: string;
  Scopes?: string;
  TokenType?: string;
  CharacterOwnerHash?: string;
  ClientID?: string;
}

export interface ESICharacterIdentifier {
  id?: number;
}

export interface ESICharacter {
  name: string;
  race_id: number;
  birthday: string;
  bloodline_id: number;
  gender: "male" | "female";

  description?: string;
  corporation_id?: number;
  alliance_id?: number;
  title?: string;

  faction_id?: number;
  security_status?: number;
}

export type Character = ESICharacter & Required<ESICharacterIdentifier>;

export interface ThirdPartyHealthcheck {
  endpoint: string;
  alive: boolean;
  error: Error | null;
  latency: number | null;
}
