import { service as cache } from "../../../caching";
import { randomUUID as uuid } from "crypto";
import Boom from "@hapi/boom";

import {
  getEsiStateKey,
  OAUTH_URI,
  fetch_token,
  verify_credentials,
  esi_healthcheck,
} from "./actions";
import { store_token } from "./database";
import { HandlerDecorations, Lifecycle } from "@hapi/hapi";

export const authenticate: Lifecycle.Method | HandlerDecorations = async (
  _req,
  h
) => {
  const state = uuid();
  await cache.set(getEsiStateKey(state), state, { EX: 60 * 5 });
  return h.redirect(
    `${OAUTH_URI}/authorize?response_type=code&redirect_uri=${encodeURI(
      process.env.ESI_REDIRECT_URI ||
        "http://localhost:8000/authentication/esi/callback"
    )}&client_id=${process.env.ESI_CLIENT_ID || ""}&state=${state}`
  );
};

export const callback: Lifecycle.Method | HandlerDecorations = async (
  req,
  _h
) => {
  const { code, state, error } = req.query;
  const stateExists = Boolean(await cache.exists(getEsiStateKey(state)));
  if (!stateExists) {
    throw Boom.badRequest("invalid state");
  }
  cache.del(getEsiStateKey(state));
  if (error) {
    throw Boom.badRequest(error);
  }
  if (!code) {
    throw Boom.badRequest("Missing `code` parameter");
  }
  const credentials = await fetch_token(code);
  const verification = await verify_credentials(credentials);
  await store_token(credentials, verification.CharacterID);

  try {
    // fetch character informations
    // save character in database
  } catch (err: any) {
    // failed to get the user informations or save to db
    // but auth went OK so we will just log it
    req.log(["esi", "error", "database"], {
      ...err,
      explanation:
        "An error occured while fetching or saving the character into the database",
    });
  }
};

export const healthcheck: Lifecycle.Method | HandlerDecorations = async (
  _req,
  h
) => {
  return h.response({
    self: {
      alive: true,
    },
    depends_on: ["cache", "database"],
    "third-party": {
      esi: await esi_healthcheck(),
    },
  });
};
