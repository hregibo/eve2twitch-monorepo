import { get } from "superagent";
import { BASE_URI, prepare_query } from "./actions";
import { Character } from "./interfaces";

export const get_character = async (
  character_id: number
): Promise<Character> => {
  try {
    const response = await prepare_query(
      get(`${BASE_URI}/character/${character_id}`)
    );
    if (response.statusCode === 200 && response.body) {
      return response.body as Character;
    }
  } catch (err: any) {
    // status code is not 2xx, handle errors here
  }
  throw Error("Unable to find given character");
};
