import { ServerRoute } from "@hapi/hapi";
import { authenticate, callback, healthcheck } from "./handlers";

export const routes: ServerRoute[] = [
  {
    path: "/health",
    method: "get",
    handler: healthcheck,
  },
  {
    path: "/",
    method: "get",
    handler: authenticate,
  },
  {
    path: "/callback",
    method: "post",
    handler: callback,
  },
];
