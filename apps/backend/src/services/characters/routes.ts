import { ServerRoute } from "@hapi/hapi";
import { get_one, list } from "./queries";

export const routes: ServerRoute[] = [
  {
    method: "GET",
    path: "/characters",
    // options: {
    //   auth: {
    //     scope: ["+characters.list"],
    //   },
    // },
    handler: async (_req, tools) => {
      const results = await list();
      tools
        .response(
          JSON.stringify({
            success: true,
            data: results,
          })
        )
        .code(200);
    },
  },
  {
    method: "GET",
    path: "/characters/{id}",
    // options: {
    //   auth: {
    //     scope: ["characters.get", "characters.self"],
    //   },
    // },
    handler: async (req, tools) => {
      const result = await get_one({ id: req.query.id });
      tools
        .response(
          JSON.stringify({
            success: !!result,
            data: result || null,
          })
        )
        .code(result ? 200 : 404);
    },
  },
];
export default routes;
