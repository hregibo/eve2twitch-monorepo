import { query } from "@hregibo/pg-dbc";
import { Character } from "../authentication/esi/interfaces";

export const list = async () => {
  const r = await query<Character>(`SELECT * FROM characters`, []);
  return r.rows;
};

export const create = async (payload: Character) => {
  await query<Character>(`INSERT INTO characters (id, name) VALUES ($1, $2);`, [
    payload.id,
    payload.name,
  ]);
};

export const get_one = async (record: Record<string, string>) => {
  const kv = [record.key, record.value];
  if (!kv) {
    throw Error(
      "Please provide a record with a character property as string, and a value as string."
    );
  }
  const r = await query<Character>(
    "SELECT * FROM characters WHERE $1 = $2 LIMIT 1",
    [kv[0], kv[1]]
  );
  return r.rows.at(0);
};
